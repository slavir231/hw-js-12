// let btns = document.querySelectorAll('.btn')
// document.addEventListener('keydown', (e) => {
//     for(let btn of btns){
//         btn.classList.remove('active')
//         if (btn.dataset.key === e.key)
//             btn.classList.add('active')
//     }
// })


//
// 1.Чому для роботи з input не рекомендується використовувати клавіатуру?


// Події input використовуються для будь-якого введення. Ми повинні використовувати події клавіатури, коли нам дійсно потрібна клавіатура

let buttons = document.querySelectorAll('.btn')

buttons.forEach(btn => {
    document.addEventListener('keydown', (ev) => {
        btn.classList.remove('active')
        if (btn.dataset.key === ev.key){
            btn.classList.add('active');
        }
    })
});


